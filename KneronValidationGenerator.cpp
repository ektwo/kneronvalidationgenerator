#include "stdafx.h"
#include <ctime>
#include <stdlib.h>
#include <cstring>
#include <comdef.h>

typedef unsigned char uint8_t;


void CreateValidationFile(const TCHAR *validationFileName,
                          const TCHAR *validationString,
                          int expirationPeriod,
                          int randomLength)
{
    FILE *p;
    if (!_tfopen_s(&p, validationFileName, L"wb"))
    {
        srand(time(NULL));

        for (int i = 0; i < randomLength; ++i)
        {
            uint8_t randomData = (uint8_t)(rand() % 255);
            fwrite((const void*)&randomData, 1, 1, p);
        }

        for (int i = 0; i < _tcslen(validationString); ++i)
        {
            fwrite((const void*)&validationString[i], 1, 1, p);
        }

        for (int i = 0; i < sizeof(expirationPeriod); ++i)
        {
            uint8_t data = (uint8_t)(expirationPeriod >> (8 * (3 - i)));
            fwrite((const void*)&data, 1, 1, p);
        }

        for (int i = 0; i < randomLength; ++i)
        {
            unsigned char randomData = (uint8_t)(rand() % 255);
            fwrite((const void*)&randomData, 1, 1, p);
        }

        fclose(p);
    }

    FILE *pConfigFile;
    if (!fopen_s(&pConfigFile, "expiration_config.h", "w"))
    {
        char szTmp[256];

        sprintf_s(szTmp, "%s\n", "#ifndef EXPIRATION_CONFIG_H");
        fwrite((void const*)szTmp, 1, strlen(szTmp), pConfigFile);
        sprintf_s(szTmp, "%s\n", "#define EXPIRATION_CONFIG_H");
        fwrite((void const*)szTmp, 1, strlen(szTmp), pConfigFile);
        sprintf_s(szTmp, "\n");
        fwrite((void const*)szTmp, 1, strlen(szTmp), pConfigFile);

        const WCHAR* wValidationFileName = validationFileName;
        _bstr_t bwValidationFileName(wValidationFileName);
        const char* cwValidationFileName = bwValidationFileName;
        sprintf_s(szTmp, "static const TCHAR *k_validation_file_name = L\"%s\";\n", cwValidationFileName);
        fwrite((void const*)szTmp, 1, strlen(szTmp), pConfigFile);

        const WCHAR* wValidationString = validationString;
        _bstr_t bwValidationString(wValidationString);
        const char* cwValidationString = bwValidationString;
        sprintf_s(szTmp, "static const TCHAR *k_validation_str = L\"%s\";\n", cwValidationString);
        fwrite((void const*)szTmp, 1, strlen(szTmp), pConfigFile);

        sprintf_s(szTmp, "static const int k_random_cnt = %d;\n", randomLength);
        fwrite((void const*)szTmp, 1, strlen(szTmp), pConfigFile);

        sprintf_s(szTmp, "\n");
        fwrite((void const*)szTmp, 1, strlen(szTmp), pConfigFile);

        sprintf_s(szTmp, "%s\n", "#endif");
        fwrite((void const*)szTmp, 1, strlen(szTmp), pConfigFile);
    }
}

int _tmain(int argc, TCHAR* argv[])
{
    CreateValidationFile(argv[1], argv[2], _ttoi(argv[3]), _ttoi(argv[4]));
    for (int i = 0; i < argc; i++)
        printf("main argv[i]=%s", argv[i]);

    return 0;
}

